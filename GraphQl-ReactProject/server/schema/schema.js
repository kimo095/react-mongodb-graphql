const graphql = require('graphql');

//import database schema

const Book = require('../models/book');
const Author = require('../models/author');


const _ = require('lodash');

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull
} = graphql;

//DUMMY DATA
// var books = [
//     {name:'The wind' , genre:'Fantasy' , id:'1',authorId:'1'},
//     {name:'Final Empire' , genre:'Fantasy' , id:'2' ,authorId:'2'},
//     {name:'The Long Earth' , genre:'Sci-Fi' , id:'3' ,authorId:'3'},
//     {name:'The wind two' , genre:'Fantasy' , id:'1',authorId:'1'},
//     {name:'Final Empire two' , genre:'Fantasy' , id:'2' ,authorId:'2'},
//     {name:'The Long Earth two' , genre:'Sci-Fi' , id:'3' ,authorId:'3'}
// ];

// var authors = [
//     {name:'Abdekareem' , age:44 , id:'1'},
//     {name:'Abdallah' , age:34 , id:'2'},
//     {name:'Jebreel' , age:24 , id:'3'},
// ]


const BookType = new GraphQLObjectType({
    name:'Book',
    fields:()=>({
        id:{type:GraphQLID},//sometimes it is better to use GraphQlID instead of GraphQlString because if you pass id as string or intger it will work
        name:{type:GraphQLString},
        genre:{type:GraphQLString},
        author:{
            type:AuthorType,
            resolve(parent,args){
             // return _.find(authors,{id:parents.authorId})
             return Author.findById(parent.authorId)
            }
        }
    })
})

const AuthorType = new GraphQLObjectType({
    name:'Author',
    fields:()=>({
        id:{type:GraphQLID},//sometimes it is better to use GraphQlID instead of GraphQlString because if you pass id as string or intger it will work
        name:{type:GraphQLString},
        age:{type:GraphQLInt},
        books:{
            type: new GraphQLList(BookType),
            resolve(parent,args){
             //   return _.filter(books,{authorId:parent.id})
             return Book.find({authorId:parent.id})
            }
        }
    })
})

const RootQuery = new GraphQLObjectType({
    name:'RootQueryType',
    fields:{
        book:{
            type:BookType,
            args:{id:{type:GraphQLID}},
            resolve(parent,args){
                //code to get data from db / other source
              // return _.find(books,{id:args.id});
              return Book.findById(args.id)
            }
        },
        author:{
            type:AuthorType,
            args:{id:{type:GraphQLID}},
            resolve(parent,args){
                //code to get data from db / other source
               // return _.find(authors,{id:args.id});
               return Author.findById(args.id)
            }
        },
        books:{
            type:new GraphQLList(BookType),
            resolve(parent,args){
                return Book.find({})
            }
        },
        authors:{
            type:new GraphQLList(AuthorType),
            resolve(parent,args){
                return Author.find({})
            }

        }
    }
});

// book(id:"2"){
//     name
//     genre 
// }

const Mutation = new GraphQLObjectType({
    name:'Mutation',
    fields:{
        addAuthor:{
            type:AuthorType,
            args:{
                name:{type: new GraphQLNonNull(GraphQLString)},
                age:{type: new GraphQLNonNull(GraphQLInt)}
            },
            resolve(parent,args){
                let author = new Author({
                    name:args.name,
                    age:args.age
                });
               return author.save()
            }
        },
        addBook:{
            type:BookType,
            args:{
                name:{type: new GraphQLNonNull(GraphQLString)},
                genre:{type: new GraphQLNonNull(GraphQLString)},
                authorId:{type: new GraphQLNonNull(GraphQLID)}
            },
            resolve(parent,args){
                let book = new Book({
                    name:args.name,
                    genre:args.genre,
                    authorId:args.authorId
                });
               return book.save()
            }
        },
    }
})

module.exports = new GraphQLSchema({
    query:RootQuery,
    mutation:Mutation
});